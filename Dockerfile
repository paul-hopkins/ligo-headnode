FROM ligo/software:stretch

LABEL name="LIGO Software Headnode Environment for Debian 9 'stretch'" \
      maintainer="Paul Hopkins <paul.hopkins@ligo.org>" \
      date="2018-08-03" \
      support="Reference Platform"

RUN apt-get update && \
    apt-get install --assume-yes \
    ldg-client \
    emacs-nox \
    vim \
    quota \
    ffmpeg && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN pip install ipykernel && \
    pip3 install ipykernel
